import React from 'react'

const Note = ({children, ...props}) => (
  <div {...props}>
    {children}
  </div>
)
Note.propTypes = {
  children: React.PropTypes.node
}
export default Note
