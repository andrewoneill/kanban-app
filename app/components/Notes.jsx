import React from 'react'
import Note from './Note'
import Editable from './Editable'

const Notes = ({
  notes,
  onNoteClick = () => {}, onEdit = () => {}, onDelete = () => {}}) => (
  <ul className="notes">{notes.map(({id, editing, task}) =>
    <li key={id}>
      <Note className="note" onClick={onNoteClick.bind(null, id)}>
        <Editable className="editable" editing={editing} value={task} onEdit={onEdit.bind(null, id)} />
        <button className="delete" onClick={onDelete.bind(null, id)}>x</button>
      </Note>
    </li>
  )}</ul>
)
Notes.propTypes = {
  notes: React.PropTypes.array,
  onNoteClick: React.PropTypes.func,
  onEdit: React.PropTypes.func,
  onDelete: React.PropTypes.func
}

export default Notes
